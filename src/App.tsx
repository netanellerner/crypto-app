import React from 'react';
import {LoginForm} from "./components/auth/login-form/login-form.component";
import './index.css';

function App() {
  return (
      <LoginForm/>
  );
}

export default App;
